<?php
namespace App\Service;

use App\Repository\LocationRepository;
use App\Entity\Location;
use App\Repository\MeasurementRepository;
use App\Entity\Measurement;

class WeatherUtil
{

    private LocationRepository $cityRepository;
    private MeasurementRepository $measuerementRepository;

    /**
    * @param $cityRepository
    * @param $measuerementRepository
    */
    function __construct(LocationRepository $cityRepository, MeasurementRepository $measuerementRepository)
    {
        $this->cityRepository = $cityRepository;
        $this->measuerementRepository = $measuerementRepository;
    }

    /**
    * @param string $country
    * @param string $name
    */
    public function getWeatherForCountryAndCity($country, $name)
    {
        $city = $this->cityRepository->findByLocationAndCountry($country, $name);
        return $this->getWeatherForLocation($city);
    }

     /**
     * @param Location $city
     */
    public function getWeatherForLocation($city)
    {
        return $this->measuerementRepository->findByLocation($city);
    }

    public function getWeatherForLocationID($id){
        $location = $this->cityRepository->findOneByID($id);
        if($location){
        $measurements = $this->measuerementRepository->findByLocation($location);
        return $measurements;
        }
        return 0;
    }
}