<?php

namespace App\Command;
use App\Service\WeatherUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CommandById extends Command{
    /** @var WeatherUtil */
    private $weatherutil;

    public function __construct(string $name=null,WeatherUtil $weatherutil){
        $this->weatherutil = $weatherutil;
        parent::__construct($name);
    }

    protected function configure():void{
        $this->setName('weather:commandbyid')
        ->addArgument('id',InputArgument::REQUIRED,'location ID?');
    }
    protected function execute(InputInterface $input,OutputInterface $output): int{
        $measurements=$this->weatherutil->getWeatherForLocationID($input->getArgument('id'));       
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($measurements, 'json');
        $output->writeln($jsonContent);
        return 1;
    }
}