<?php

namespace App\Repository;

use App\Entity\Location;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Location|null find($id, $lockMode = null, $lockVersion = null)
 * @method Location|null findOneBy(array $criteria, array $orderBy = null)
 * @method Location[]    findAll()
 * @method Location[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Location::class);
    }

    // /**
    //  * @return Location[] Returns an array of Location objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Location
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
     /**
     * @param $city
     */

    public function findLocationByName($city)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.city = :city')
        ->setParameter('city', $city);
        $query = $qb->getQuery();
        $result = $query->getResult();
        return $result;
    }

    /**
     * @param $country
     * @param $city
     */

    public function findByLocationAndCountry($country, $city)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.country = :country')
        ->setParameter('country', $country)
        ->andWhere('m.city = :city')
        ->setParameter('city', $city);
        $query = $qb->getQuery();
        return $query->getSingleResult();
    }

    public function findOneByID($id): ?Location
{
    return $this->createQueryBuilder('l')
        ->andWhere('l.id = :id')
        ->setParameter('id', $id)
        ->getQuery()
        ->getOneOrNullResult()
    ;
}
}

